﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Otus.Teaching.Concurrency.Import.SerializerDeserializer
{
    public static class Deserializer<T>
    {
        public static List<T> Deserialize(string str)
        {
            var res = new List<T>();

            var list = str.Split("\r\n");

            var names = list[0].Split(';');

            var newType = typeof(T);

            for (var i = 1; i < list.Length - 1; i++)
            {
                var objectValues = list[i].Split(';');

                var instance = Activator.CreateInstance(newType);

                for (var j = 0; j < names.Length; j++)
                {
                    var field = newType.GetField(names[j]);

                    field.SetValue(instance, ConvertToType(field, objectValues[j]));
                }
                res.Add((T)instance);
            }

            return res;
        }

        private static dynamic ConvertToType(FieldInfo field, string value)
        {
            if (field.FieldType == typeof(int))
            {
                return int.Parse(value);
            }

            if (field.FieldType == typeof(string))
            {
                return value;
            }

            return value;
        }
    }


}
