﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.SerializerDeserializer
{
    public static class Serializer<T>
    {
        public static string Serialize(T obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj));

            sb.AppendLine(GetObjectLine(obj));

            return sb.ToString();
        }

        public static string Serialize(T[] obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj[0]));

            foreach (var ob in obj)
            {
                sb.AppendLine(GetObjectLine(ob));
            }
            return sb.ToString();
        }

        public static string Serialize(List<T> obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj[0]));

            foreach (var ob in obj)
            {
                sb.AppendLine(GetObjectLine(ob));
            }
            return sb.ToString();
        }

        public static void Serialize(FileStream stream, List<T> obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj[0]));

            foreach (var ob in obj)
            {
                sb.AppendLine(GetObjectLine(ob));
            }

            byte[] buffer = Encoding.Default.GetBytes(sb.ToString());

            stream.WriteAsync(buffer, 0, buffer.Length);
        }

        private static string GetHeaderLine(T obj)
        {
            var sb = new StringBuilder();

            var myType = obj.GetType();

            var fields = myType.GetFields();

            foreach (var field in fields)
            {
                _ = sb.Length == 0 ? sb.Append(field.Name) : sb.Append(";" + field.Name);
            }
            return sb.ToString();
        }

        private static string GetObjectLine(T obj)
        {
            var sb = new StringBuilder();

            var myType = obj.GetType();

            var fields = myType.GetFields();

            foreach (var field in fields)
            {
                _ = sb.Length == 0 ? sb.Append(field.GetValue(obj)) : sb.Append(";" + field.GetValue(obj));
            }
            return sb.ToString();
        }

    }
}
