using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlGenerator : IDataGenerator
    {
        private readonly string _fileName;

        private readonly int _dataCount;

        private CustomersList _customersList = new CustomersList() { Customers = new List<Customer>() };

        public XmlGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;

            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);

            using (var stream = File.Create(_fileName))
            {
                new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
                {
                    Customers = customers
                });
            }

        }

        public void GenerateWithThreads(int ThreadNumber)
        {
            int threadDataCount = _dataCount / ThreadNumber;
            int startId = 1;

            for (int i = 0; i < ThreadNumber; i++)
            {
                var thread = new Thread(() =>
                {
                    GenerateWithThread(threadDataCount, startId);
                });

                thread.Start();

                thread.Join();

                startId += threadDataCount;
            }

            using var stream = File.Create(_fileName);

            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = _customersList.Customers
            });
        }

        public void GenerateWithThreadpool(int ThreadNumber)
        {

            int threadDataCount = _dataCount / ThreadNumber;

            int startId = 1;

            ManualResetEvent[] handles = new ManualResetEvent[ThreadNumber];

            for (int i = 0; i < ThreadNumber; i++)
            {
                Dictionary<string, object> ThreadInfo = new Dictionary<string, object>();

                ThreadInfo["threadDataCount"] = threadDataCount;

                ThreadInfo["startId"] = startId;

                ThreadInfo["handleNum"] = i;

                handles[i] = new ManualResetEvent(false);

                ThreadPool.QueueUserWorkItem(new WaitCallback(x =>
                {
                    GenerateWithThread((int)ThreadInfo["threadDataCount"], (int)ThreadInfo["startId"]);
                    handles[(int)ThreadInfo["handleNum"]].Set();
                }), ThreadInfo);

                startId += threadDataCount;
            }

            WaitHandle.WaitAll(handles);

            using var stream = File.Create(_fileName);

            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = _customersList.Customers
            });
        }

        public void GenerateWithThread(int ThreadDataCount, int StartId)
        {
            var customers = RandomCustomerGenerator.Generate(ThreadDataCount, StartId);

            _customersList.Customers.AddRange(customers);
        }
    }
}