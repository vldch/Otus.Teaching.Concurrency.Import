﻿using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Xml.Serialization;
using System.Linq;
using System;
using System.Text;
using System.Threading;


namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _fileName;

        public XmlParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {
            var customers = new List<Customer>();

            using (var fs = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var reader = new StreamReader(fs))
                {

                    var customerV = (Customer)new XmlSerializer(typeof(Customer)).Deserialize(fs);

                    customers.Add(customerV);
                }
            }

            return customers;
        }

        public List<Customer> ParseString()
        {
            var customers = new List<Customer>();

            using (StreamReader str = new StreamReader(_fileName, Encoding.Default))
            {

                const int threadsCount = 4;

                var lineCount = 0;

                while ((_ = str.ReadLine()) != null)
                {
                    lineCount++;
                }

                var recordCount = (lineCount - 5) / 6;

                var chunkLength = (int)Math.Ceiling(recordCount / (double)threadsCount);

                for (int i = 3; i < lineCount - 2; i += 6)
                {
                    var lines = string.Join("", File.ReadLines(_fileName).Skip(i).Take(6).ToArray());

                    using (TextReader reader = new StringReader(lines))
                    {
                        var customerV = (Customer)new XmlSerializer(typeof(Customer)).Deserialize(reader);

                        customers.Add(customerV);
                    }
                }
            }

            return customers;
        }

        public class CustomerChunkParameters
        {
            public StreamReader Stream { get; set; }
            public int StartIndex { get; set; }
            public int ChunkLength { get; set; }
        }
    }
}