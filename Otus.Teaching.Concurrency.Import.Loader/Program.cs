﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            Console.WriteLine("Enter switch number where: number 1 is no threads, number 2 is threads, number 3 is threadpool");

            var switcher = int.Parse(Console.ReadLine());

            GenerateCustomersDataFile(switcher);
            
            var loader = new FakeDataLoader();

            loader.LoadData();
        }

        static void GenerateCustomersDataFile(int switcher)
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);

            switch (switcher)
            {
                case 1:
                    xmlGenerator.Generate();
                    break;
                case 2:
                    xmlGenerator.GenerateWithThreads(4);
                    break;
                case 3:
                    xmlGenerator.GenerateWithThreadpool(4);
                    break;
            }
        }
    }
}