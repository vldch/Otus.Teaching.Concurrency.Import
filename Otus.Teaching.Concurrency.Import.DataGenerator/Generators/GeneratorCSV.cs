﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.SerializerDeserializer;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class GeneratorCSV : IDataGenerator
    {
        private readonly string _fileName;

        private readonly int _dataCount;

        public GeneratorCSV(string fileName, int dataCount)
        {
            _fileName = fileName;

            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);

            using var stream = File.Create(_fileName);

            Serializer<Customer>.Serialize(stream, customers);
        }
    }
}
