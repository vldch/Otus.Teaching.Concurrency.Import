﻿using System;
using System.IO;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;

        private static string _dataFileName; 

        private static int _dataCount = 100; 
        
        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            Console.WriteLine("Generating xml data...");

            var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount);
            
            generator.Generate();
            
            Console.WriteLine($"Generated xml data in {_dataFileName}...");

            var XmlParser = new XmlParser(_dataFileName);

            var customer = XmlParser.ParseString();
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            _dataFileName = Path.Combine(_dataFileDirectory, "qqqqq.xml");

            if (args.Length > 1)
            {

                if (!int.TryParse(args[1], out _dataCount))
                {

                    Console.WriteLine("Data must be integer");

                    return false;
                }
            }

            return true;
        }
    }
}